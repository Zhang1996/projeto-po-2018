/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MB;

import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Cliente;
import modelo.ItemPedido;
import modelo.Pedido;
import modelo.Produto;
import service.PedidoDados;

/**
 *
 * @author 150499
 */
@ManagedBean
@SessionScoped
public class PedidoMB {
    private PedidoDados listaPedidos = new PedidoDados();
    private Pedido x = new Pedido();
    private ItemPedido item = new ItemPedido();
    private Cliente seleclient;
    
    public void addItem(){
        x.addItem(item);
        item = new ItemPedido();
    }
   
    public void addPedido(){
        x.setCliente(seleclient);
        listaPedidos.addpedido(x);
        x = new Pedido();
    }

    public ArrayList<Pedido> getListaPedidos() {
        return listaPedidos.getListaPedidos();
    }

    public void setListaPedidos(PedidoDados listaPedidos) {
        this.listaPedidos = listaPedidos;
    }

    public Pedido getX() {
        return x;
    }

    public void setX(Pedido x) {
        this.x = x;
    }

    public ItemPedido getPed() {
        return item;
    }
    public ArrayList<ItemPedido> getListaItens(){
        return x.getItens();
    }
    
    public Cliente getSeleclient() {
        return seleclient;
    }

    public void setSeleclient(Cliente seleclient) {
        this.seleclient = seleclient;
    }
    
}
