/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MB;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Cliente;
import modelo.Pedido;
import service.ClienteDados;

@ManagedBean
@SessionScoped
public class ClienteMB {
    private ClienteDados clientinfo = new ClienteDados();
    private Cliente client = new Cliente();
    private Cliente selectedclient;
    
    public void addcliente(){
        clientinfo.addcliente(client);
        client = new Cliente();
    }

    public ClienteDados getClientinfo() {
        return clientinfo;
    }

    public void setClientinfo(ClienteDados clientinfo) {
        this.clientinfo = clientinfo;
    }

    public Cliente getClient() {
        return client;
    }

    public void setClient(Cliente client) {
        this.client = client;
    }


    public ArrayList<Cliente> getListaClientes(){
        return clientinfo.getListaCliente();
    }
    public String vaiPedido(){
        System.out.println("MB.ClienteMB.vaiPedido()");
        return "CadastroPedido";
    }

    public Cliente getSelectedclient() {
        return selectedclient;
    }

    public void setSelectedclient(Cliente selectedclient) {
        this.selectedclient = selectedclient;
    }
    
}