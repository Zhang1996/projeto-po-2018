/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MB;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Produto;
import service.ProdutoDados;



@ManagedBean
@SessionScoped
public class ProdutoMB {
    private ProdutoDados listaProduto = new ProdutoDados();
    private Produto prod = new Produto();
    private Produto selectedprod;
    public void salvax(){
        listaProduto.add(prod);
        prod = new Produto();
    }
    
    public ArrayList<Produto> getListaProduto(){
        return listaProduto.getProdutos();
    }
    public Produto getProd(){
        return prod;
    }
    public void setProd(Produto prod){
        this.prod = prod;
    }

    public Produto getSelectedprod() {
        return selectedprod;
    }

    public void setSelectedprod(Produto selectedprod) {
        this.selectedprod = selectedprod;
    }
    
}
