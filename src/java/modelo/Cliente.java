
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import service.DadosGerais;

@Entity
@Table (name = "TBL_Cliente")
public class Cliente implements Serializable{
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int codigo;
    private String nome;
    private String endereco;
    private String telefone;
    private int status;
    @Transient
    private double limite;
    @OneToMany
    private ArrayList<Pedido> pedidos = new ArrayList();
    private DadosGerais a = new DadosGerais();
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    public ArrayList<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(ArrayList<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    public Cliente() {
        a.incCliente();
        this.codigo = DadosGerais.getIdCliente();
    }
    
}
