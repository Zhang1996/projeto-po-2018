/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import service.DadosGerais;

/**
 *
 * @author 150499, 160066, 160038
 */

@Entity
@Table ( name = "TBL_Pedidos")
public class Pedido implements Serializable{
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private long numero;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;
    @ManyToMany
    private ArrayList<ItemPedido> itens = new ArrayList();
    @ManyToOne
    private Cliente cliente;
    private DadosGerais a = new DadosGerais();
    public double totalPedido(){
        return 1;
    }
    public double totalImposto(){
        return 1;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<ItemPedido> getItens() {
        return itens;
    }

    public void setItens(ArrayList<ItemPedido> itens) {
        this.itens = itens;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    public void addItem(ItemPedido x){
        itens.add(x);
    }

    public Pedido() {
        a.incPed();
        this.numero = DadosGerais.getIdPed();
    }
    
}
