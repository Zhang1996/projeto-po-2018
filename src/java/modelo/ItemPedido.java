
package modelo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import service.DadosGerais;

@Entity
@Table (name = "TBL_ItemPedido")
public class ItemPedido implements Serializable{
    
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int numero;
    private int quantidade;
    @OneToOne
    @JoinTable (name="Id_Produto")
    private Produto produto;
    private final DadosGerais a = new DadosGerais();
    public double totalItem(){
        return (produto.getPreco() * quantidade);
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public ItemPedido() {
        a.incItem();
        this.numero = DadosGerais.getIdItem();
    }
    
}
