
package service;

import java.util.ArrayList;
import modelo.Produto;


public class ProdutoDados {
    private ArrayList<Produto> list = DadosGerais.getListproduto();
    
    public ArrayList<Produto> getProdutos() {
        return list;
    }
    public void add(Produto x){
        list.add(x);
    }
    public void removeproduto(Produto x){
        list.remove(x);
    }
    public Produto getProdutoByNome(String value) {
    
        for(Produto e: list){
           if(e.getNome().equals(value))
               return e;
        
        }
        
        return null;
    }
}
