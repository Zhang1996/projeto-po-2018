/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import modelo.Cliente;

/**
 *
 * @author 150499
 */
public class ClienteDados {
    private ArrayList<Cliente> listaCliente = DadosGerais.getClientes();
    
    public void addcliente(Cliente x){
        listaCliente.add(x);
    }

    public ArrayList<Cliente> getListaCliente() {
        return listaCliente;
    }

    public void setListaCliente(ArrayList<Cliente> listaCliente) {
        this.listaCliente = listaCliente;
    }
    public void removecliente(Cliente x){
        listaCliente.remove(x);
    }
    public Cliente getClienteByNome(String value) {
    
        for(Cliente e: listaCliente){
           if(e.getNome().equals(value))
               return e;
        
        }
        
        return null;
    }
}
