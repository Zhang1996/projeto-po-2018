/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import modelo.Cliente;
import modelo.Pedido;
import modelo.Produto;

/**
 *
 * @author 150499
 */
public class DadosGerais {
    private static ArrayList<Produto> listproduto = new ArrayList();
    private static ArrayList<Cliente> listcliente = new ArrayList();
    private static ArrayList<Pedido> listpedido = new ArrayList();
    private static Produto a = new Produto();
    private static Cliente b = new Cliente();
    private static Pedido c = new Pedido();
    private static int idPed = 0;
    private static int idCliente = 0;
    private static int idItem = 0;
    private static int idProd = 0;
    

    public static ArrayList<Produto> getListproduto() {
        if (listproduto == null) {
            listproduto.add(a);
        }
        return listproduto;
    }

    public static void setListproduto(ArrayList<Produto> listproduto) {
        DadosGerais.listproduto = listproduto;
    }

    public static ArrayList<Cliente> getClientes() {
        if (listcliente == null) {
            listcliente.add(b);
        }
        return listcliente;
    }

    public static void setListcliente(ArrayList<Cliente> listcliente) {
        DadosGerais.listcliente = listcliente;
    }

    public static ArrayList<Pedido> getListpedido() {
        if (listpedido == null) {
            listpedido.add(c);
        }
        return listpedido;
    }

    public static void setListpedido(ArrayList<Pedido> listpedido) {
        DadosGerais.listpedido = listpedido;
    }
    public void addcli(Cliente x){
        listcliente.add(x);
    }
    public void addped(Pedido x){
        listpedido.add(x);
    }
    public void addprod(Produto x){
        listproduto.add(x);
    }

    public static int getIdPed() {
        return idPed;
    }

    public static int getIdCliente() {
        return idCliente;
    }

    public static int getIdItem() {
        return idItem;
    }

    public static int getIdProd() {
        return idProd;
    }
    public void incCliente(){
        idCliente++;
    }
    public void incItem(){
        idItem++;
    }
    public void incPed(){
        idPed++;
    }
    public void incProd(){
        idProd++;
    }
}
